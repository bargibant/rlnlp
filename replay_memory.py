"""Replay memory.
This class will register successful episodes and replay them once in a while.
Frequence of replay should be proportional to the mean state value.

Class is inspired from DQN Tutorial on PyTorch.

"""
import torch
import numpy as np
import heapq
from collections import namedtuple

Transition = namedtuple('Transition', ('src', 'tgt', 'action', 'next_tgt', 'reward'))


class ReplayMemoryBis():
    """Limited memory to register episodes and replay it."""

    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.weights = []
        self.position = 0

    def push(self, transition, weight=0):
        """Saves an episode"""
        if len(self.memory) < self.capacity:
            self.memory.append(None)
            self.weights.append(None)
        self.memory[self.position] = transition
        self.weights[self.position] = 1
        self.position = (self.position+1) % self.capacity

    def sample(self, batch_size):
        """Sample episodes uniformly on memory."""

        indices = range(len(self))
        probability = np.array(self.weights)
        probability = probability/np.sum(probability)
        indices = np.random.choice(
            indices, size=batch_size, replace=False, p=probability)
        batch = [self.memory[i] for i in indices]
        return batch

    def flush(self):
        """Delete memory."""
        self.memory = []
        self.weights = []
        self.position = 0

    def __len__(self):
        return len(self.memory)

class BalancedMemory():
    """Balanced memory to give balanced batch."""

    def __init__(self, capacity):
        self.memory_success = ReplayMemory(capacity//2)
        self.memory_fail = ReplayMemory(capacity-capacity//2)

    def push_success(self, agent_input, agent_actions, agent_reward, agent_value):
        """Saves successful episodes."""
        self.memory_success.push(
            agent_input, agent_actions, agent_reward, agent_value)

    def push_fail(self, agent_input, agent_actions, agent_reward, agent_value):
        """Saves failed episodes."""

        self.memory_fail.push(agent_input, agent_actions,
                              agent_reward, agent_value)

    def flush(self):
        """Flush successful memory,
        to prevent potential overfitting from old samples."""
        self.memory_success.flush()

    def sample(self, batch_size, ratio):
        """Sample episodes uniformly on memory."""

        success_size = round(ratio*batch_size)
        success_size = min(len(self.memory_success), success_size)
        fail_size = min(len(self.memory_fail), success_size,
                        batch_size-success_size)

        # Poor syntax but torch.cat cannot accept None list...
        input_list = []
        action_list = []
        reward_list = []
        if success_size > 0:
            input_success, action_success, reward_success = self.memory_success.sample(
                success_size)
            input_list.append(input_success)
            action_list.append(action_success)
            reward_list.append(reward_success)
        if fail_size > 0:
            input_fail, action_fail, reward_fail = self.memory_fail.sample(
                fail_size)
            input_list.append(input_fail)
            action_list.append(action_fail)
            reward_list.append(reward_fail)

        input_batch = torch.cat(input_list, dim=-1)
        action_batch = torch.cat(action_list, dim=-1)
        reward_batch = torch.cat(reward_list, dim=-1)

        return input_batch, action_batch, reward_batch

    def __len__(self):
        return len(self.memory_success)


class ReplayMemory():
    """Limited memory to register episodes and replay it."""

    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.weights = []
        self.step = 0
        self.positions = []
        self.position = 0

    def push(self, agent_input, agent_actions, agent_reward, agent_value=0):
        """Saves an episode"""
        if len(self.memory) < self.capacity:
            self.memory.append(None)
            self.weights.append(None)
            position = self.step % self.capacity
        else:
            _, position = heapq.heappop(self.positions)

        self.memory[position] = (agent_input.clone(),
                                      agent_actions.clone(),
                                      agent_reward.clone())
        self.weights[position] = (
            (agent_value.clone()-agent_reward.clone())**2).item()
        heapq.heappush(self.positions, (-self.weights[position], position))
        self.step += 1

    def sample(self, batch_size):
        """Sample episodes uniformly on memory."""
        indices = range(len(self))
        probability = np.array(self.weights)
        probability = probability/np.sum(probability)
        indices = np.random.choice(
            indices, size=batch_size, replace=False, p=probability)
        batch = [self.memory[i] for i in indices]
        input_batch, action_batch, reward_batch = [], [], []
        for agent_input, actions, reward in batch:
            input_batch.append(agent_input)
            action_batch.append(actions)
            reward_batch.append(reward)
        input_batch = torch.stack(input_batch)
        action_batch = torch.stack(action_batch)
        reward_batch = torch.stack(reward_batch)
        return input_batch.t(), action_batch.t(), reward_batch.t()

    def flush(self):
        """Delete memory."""
        self.memory = []
        self.weights = []
        self.position = 0
        self.step = 0

    def __len__(self):
        return len(self.memory)
