"""Modified Adam and SGD optimization algorithm to implement elegibility traces.

For each parameter p, traces register the current trace and this trace is used as the gradient of p.
The trace is deleted when masked is False, meaning that we operate the final transition.

When ActorCriticHandler.next_done is True, the grad is 0 (no backpropagation after the end of
the episode), so the trace is set to 0 in this case.

"""

import torch
import torch.optim as optim
import torch.optim._functional as F


class AdamElegibilityTraces(optim.Adam):
    """Adam with Elegibility Traces"""

    def __init__(self, params, lr=1e-3, betas=(0.9, 0.999), eps=1e-8, weight_decay=0, lbd=0.6, amsgrad=False):
        self.traces = {}
        self.lbd = lbd
        super().__init__(params, lr, betas, eps, weight_decay, amsgrad)

    def delete_traces(self):
        self.traces = {}

    @torch.no_grad()
    def step(self, masked=True, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            with torch.enable_grad():
                loss = closure()

        for group in self.param_groups:
            params_with_grad = []
            grads = []
            exp_avgs = []
            exp_avg_sqs = []
            max_exp_avg_sqs = []
            state_steps = []

            for p in group['params']:
                if p.grad is not None:
                    params_with_grad.append(p)
                    if p.grad.is_sparse:
                        raise RuntimeError(
                            'Adam does not support sparse gradients, please consider SparseAdam instead')

                    if p not in self.traces.keys():
                        self.traces[p] = torch.zeros_like(p.grad)

                    self.traces[p] = self.lbd*self.traces[p] + p.grad

                    if masked and torch.all(p.grad.eq(0)):
                        trace = torch.zeros_like(p.grad)
                    else:
                        trace = self.traces[p]

                    grads.append(trace)

                    state = self.state[p]
                    # Lazy state initialization
                    if len(state) == 0:
                        state['step'] = 0
                        # Exponential moving average of gradient values
                        state['exp_avg'] = torch.zeros_like(
                            p, memory_format=torch.preserve_format)
                        # Exponential moving average of squared gradient values
                        state['exp_avg_sq'] = torch.zeros_like(
                            p, memory_format=torch.preserve_format)
                        if group['amsgrad']:
                            # Maintains max of all exp. moving avg. of sq. grad. values
                            state['max_exp_avg_sq'] = torch.zeros_like(
                                p, memory_format=torch.preserve_format)

                    exp_avgs.append(state['exp_avg'])
                    exp_avg_sqs.append(state['exp_avg_sq'])

                    if group['amsgrad']:
                        max_exp_avg_sqs.append(state['max_exp_avg_sq'])

                    # update the steps for each param group update
                    state['step'] += 1
                    # record the step after step update
                    state_steps.append(state['step'])

            beta1, beta2 = group['betas']
            F.adam(params_with_grad,
                   grads,
                   exp_avgs,
                   exp_avg_sqs,
                   max_exp_avg_sqs,
                   state_steps,
                   group['amsgrad'],
                   beta1,
                   beta2,
                   group['lr'],
                   group['weight_decay'],
                   group['eps']
                   )

        if not masked:
            self.traces = {}
        return loss

class SGDElegibilityTraces(optim.SGD):
    """SGD with Elegibility Traces"""
    def __init__(self, params, lr=1e-5, momentum=0, dampening=0,
                 weight_decay=0, nesterov=False, lbd=0):
        self.traces = {}
        self.lbd = lbd
        super().__init__(params, lr=1e-5, momentum=0, dampening=0,
                 weight_decay=0, nesterov=False)

    def delete_traces(self):
        self.traces = {}

    @torch.no_grad()
    def step(self, masked=True, closure=None):
        """Performs a single optimization step.

        Args:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            with torch.enable_grad():
                loss = closure()

        for group in self.param_groups:
            params_with_grad = []
            d_p_list = []
            momentum_buffer_list = []
            weight_decay = group['weight_decay']
            momentum = group['momentum']
            dampening = group['dampening']
            nesterov = group['nesterov']
            lr = group['lr']

            for p in group['params']:
                if p.grad is not None:
                    params_with_grad.append(p)


                    if p not in self.traces.keys():
                        self.traces[p] = torch.zeros_like(p.grad)

                    self.traces[p] = self.lbd*self.traces[p] + p.grad

                    if masked and torch.all(p.grad.eq(0)):
                        trace = torch.zeros_like(p.grad)
                    else:
                        trace = self.traces[p]

                    d_p_list.append(trace)

                    state = self.state[p]
                    if 'momentum_buffer' not in state:
                        momentum_buffer_list.append(None)
                    else:
                        momentum_buffer_list.append(state['momentum_buffer'])


            F.sgd(params_with_grad,
                  d_p_list,
                  momentum_buffer_list,
                  weight_decay,
                  momentum,
                  lr,
                  dampening,
                  nesterov)

            # update momentum_buffers in state
            for p, momentum_buffer in zip(params_with_grad, momentum_buffer_list):
                state = self.state[p]
                state['momentum_buffer'] = momentum_buffer

        if not masked:
            self.traces = {}
        return loss
