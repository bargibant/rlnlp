"""Environment, just used for initial input and defining final reward."""
import random
from dataclasses import dataclass

import torch

alphabet = {
    0: '',
    1: 'a',
    2: 'b',
    3: 'c',
    4: 'd',
    5: 'e',
    6: 'f',
    7: 'g',
    8: 'h',
    9: 'i',
    10: 'j',
    11: 'k',
    12: 'l',
    13: 'm',
    14: 'n',
    15: 'o',
    16: 'p',
    17: 'q',
    18: 'r',
    19: 's',
    20: 't',
    21: 'u',
    22: 'v',
    23: 'w',
    24: 'x',
    25: 'y',
    26: 'z',
}

@dataclass(eq=False)
class Environment:
    """Provides initial input and rewards for the task.
    """
    input_max_len: int = 10

    def get_input(self, num_items):
        """ Generates input for the encoder.
        The input is a tensor of size list_max_length, but only the random first
        elements are not 0.

        Returns:
            Tensor: (list_max_length, 1) Random length list filled with random items.
        """
        list_length = random.randint(1, self.input_max_len)
        truncated_list = torch.randint(
            low=1, high=num_items+1, size=(list_length, 1))

        input_list = torch.cat([truncated_list, torch.full(
            (self.input_max_len-list_length, 1), 0)], dim=0)
        return input_list

    def get_input_batch(self, num_items, bsz=1):
        """ Generates input batch for the encoder.

        Returns:
            Tensor: (list_max_length*bsz) encoder input batch.
        """
        return torch.cat([self.get_input(num_items) for _ in range(bsz)], dim=1)

def translate(src):
    """Returns character translation of source.

    Args:
        src (Tensor): Tensor to translate.

    Returns:
        list: List of sentences.
    """
    sentences = []
    for vec in src.t():
        sentences.append(''.join([alphabet[index]
                                    for index in vec.tolist()]))
    return sentences

def contain_same_elements(input_tensor, output_tensor):
    """Checks if input and output contains same elements per batch tensor.
    Args:
        input_tensor (Tensor): input batch to encoder
        output_tensor (Tensor): output batch from decoder

    Returns:
        Tensor: Boolean tensor.
    """
    sorted_input, _ = input_tensor.sort(dim=0, descending=True)
    sorted_output, _ = output_tensor.sort(dim=0, descending=True)

    return torch.all(sorted_input.eq(sorted_output), dim=0)

def are_equal_reward(input_tensor, output_tensor, penalize=True):
    """Checks if input and output tensors are equal per batch tensor.

    Args:
        input_tensor (Tensor)
        output_tensor (Tensor)
        penalize (Bool, optional): Whether to give -1 reward when failed.

    Returns:
        Tensor: Boolean tensor.
    """
    success = torch.all(input_tensor == output_tensor, dim=0)
    reward = success.long()
    if penalize:
        reward = reward - (~success).long()
    return reward
