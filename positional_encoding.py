"""Positional Encoding
"""
import math

import torch
import torch.nn as nn


class PositionalEncoding(nn.Module):
    """
        Positional encoding such as described in this tutorial:
            https://pytorch.org/tutorials/beginner/transformer_tutorial.html
    """

    def __init__(self, d_model, dropout=0.1, max_len=100):
        super().__init__()
        self.dropout = nn.Dropout(p=dropout)

        pos_enc = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(
            0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pos_enc[:, 0::2] = torch.sin(position * div_term)
        pos_enc[:, 1::2] = torch.cos(position * div_term)
        pos_enc = pos_enc.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pos_enc', pos_enc)

    def forward(self, src):
        """Adds the positional encoding to the source tensor

        Args:
            src (Tensor): source vector

        Returns:
            Tensor: the result tensor. Same size as source vector.
        """
        src = src + self.pos_enc[:src.size(0), :].clone()
        return self.dropout(src)
