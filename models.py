""" Models for Actor and Critic.
See actor_critic_handler for list of all possible parameters detail.
"""

import torch
import torch.nn as nn

from positional_encoding import PositionalEncoding


class Actor(nn.Module):
    """ Actor model, computes action and log propability of this action
    under a log-softmax distribution.
    """

    def __init__(self, device, **kwargs):
        super().__init__()
        prop_defaults = {
            "tgt_size": 26,
            "tgt_max_len": 5,
            "embedding_dim": 128
        }
        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))

        self.device = device
        self.transformer = CustomTransformer(device, **kwargs)

        self.linear_probs = nn.Linear(
            self.embedding_dim, self.tgt_size+1, bias=False)
        # torch.nn.init.zeros_(self.linear_probs.weight)

        self.soft_max = nn.Softmax(dim=-1)

    def forward(self, src, tgt, step, epsilon):
        """Computes next action and its log probability from state (src and tgt).

        Args:
            src (Tensor): source tensor (S,N).
            tgt (Tensor): target tensor (T,N).
            step (Tensor): time-step of the current episode per batch.
            epsilon (Tensor): epsilon for epsilon-greedy sampling.

        Returns:
            (Tensor, Tensor): action (1,N) selected by log-softmax distribution,
            and its log probability (1,N).
        """
        probs = self.get_probs(src, tgt, step)
        distrib = torch.distributions.Categorical(probs)

        sample = torch.rand((1, src.size(-1))).to(self.device)

        threshold = sample > epsilon

        if torch.any(step == self.tgt_max_len):
            action = torch.zeros_like(sample, dtype=torch.long).to(self.device)
            log_prob = distrib.log_prob(action)
        else:
            # Ensure that first action is not a 0,
            # ie that the model produces at least an action before termination.
            if torch.all(step == 0).item():
                with torch.no_grad():
                    # Select action greedily according to probs.
                    first_distrib = torch.distributions.Categorical(probs[:, 1:].clone())
                    action_max = first_distrib.sample().unsqueeze(dim=0)
                    # action_max = torch.argmax(
                    #     probs[:, 1:].clone(), dim=-1).unsqueeze(dim=0)
                    action_max = action_max + 1
                    # Select action randomly.
                    action_random = torch.randint(
                        low=1, high=self.tgt_size+1, size=(1, src.size(-1))).to(self.device)
            else:
                with torch.no_grad():
                    action_max = distrib.sample().unsqueeze(dim=0)
                    # action_max = torch.argmax(probs, dim=-1).unsqueeze(dim=0)
                    action_random = torch.randint(
                        low=0, high=self.tgt_size+1, size=(1, src.size(-1))).to(self.device)
            # Select action according to the epsilon-greedy sampling.
            # if sample > epsilon, greedy action is retained, random action otherwise.
            action = action_max*threshold.long() + action_random*(~threshold).long()
            log_prob = distrib.log_prob(action)
            log_prob = ((torch.log(1-epsilon))*threshold.float()
                + torch.log(epsilon/(self.tgt_size+1))*(~threshold).float()) + log_prob

        return action, log_prob

    def get_probs(self, src, tgt, step):
        """Generates the probabilities of all possible actions according to
        a soft_max distribution.

        Args:
            src (Tensor): source tensor (S,N).
            tgt (Tensor): target tensor (T,N).
            step (Tensor): time-step of the current episode for each batch (1,N).

        Returns:
            Tensor: probabilities (N, #actions)
        """
        out = self.transformer(src, tgt, step)
        probs = self.linear_probs(out)
        probs = self.soft_max(probs)
        return probs

    def get_log_probs(self, src, tgt, step, action):
        """Computes the log probability of action.

        Args:
            src (Tensor): source tensor (S,N).
            tgt (Tensor): target tensor (T,N).
            step (Tensor): time-step of the current episode for each batch.
            action (Tensor): action (1,N).

        Returns:
            Tensor: log probability of action (1,N).
        """
        probs = self.get_probs(src, tgt, step)
        # Compute next index
        distrib = torch.distributions.Categorical(probs)
        return distrib.log_prob(action)


class Critic(nn.Module):
    """Actor model, computes state value from state (src and tgt)."""

    def __init__(self, device, **kwargs):
        super().__init__()
        prop_defaults = {
            "embedding_dim": 128
        }
        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))
        self.transformer = CustomTransformer(device, **kwargs)
        self.linear_value = nn.Linear(self.embedding_dim, 1, bias=False)
        # torch.nn.init.zeros_(self.linear_value.weight)

    def forward(self, src, tgt, step):
        """Computes next action and its log probability from state (src and tgt).

        Args:
            src (Tensor): source tensor (S,N).
            tgt (Tensor): target tensor (T,N).
            step (Tensor): time-step of the current episode for each batch.

        Returns:
            Tensor: state value (1, N)
        """
        out = self.transformer(src, tgt, step)
        value = self.linear_value(out)
        value = value.t()
        return value


class CustomTransformer(nn.Module):
    """Transformer and embedders."""

    def __init__(self, device, **kwargs):
        super().__init__()
        prop_defaults = {
            "src_size": 2,
            "tgt_size": 26,
            "src_max_len": 5,
            "tgt_max_len": 5,
            "nhead": 4,
            "embedding_dim": 128,
            "num_encoder_layers": 3,
            "num_decoder_layers": 3,
            "dim_feedforward": 512,
            "dropout": 0.0
        }
        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))

        self.device = device
        # layers
        self.src_emb = nn.Embedding(
            self.src_size+1, self.embedding_dim, 0)

        self.tgt_emb = nn.Embedding(
            self.tgt_size+2, self.embedding_dim, 0)

        self.src_positional_encoding = PositionalEncoding(
            self.embedding_dim, self.dropout, max_len=self.src_max_len
        )

        self.tgt_positional_encoding = PositionalEncoding(
            self.embedding_dim, self.dropout, max_len=self.tgt_max_len+2
        )

        self.transformer = nn.Transformer(
            d_model=self.embedding_dim,
            nhead=self.nhead,
            num_encoder_layers=self.num_encoder_layers,
            num_decoder_layers=self.num_decoder_layers,
            dim_feedforward=self.dim_feedforward,
            dropout=self.dropout)


    def forward(self, src, tgt, step):
        """Returns transformer output according to each batch step.

        Args:
            src (Tensor): source tensor (S,N).
            tgt (Tensor): target tensor (T,N).
            step (Tensor): time-step of the current episode for each batch.

        Returns:
            Tensor: Transformer output.
        """
        src_key_padding_mask = (src == 0).t().to(self.device)
        src = self.get_src_embedding(src)
        tgt_key_padding_mask = (tgt == -1).to(self.device)
        tgt_key_padding_mask[0, :] = False
        tgt_key_padding_mask = tgt_key_padding_mask.t().clone()
        tgt = self.get_tgt_embedding(tgt)
        tgt_mask = self.transformer.generate_square_subsequent_mask(
            tgt.size(0)).clone().to(self.device)
        out = self.transformer.forward(src=src,
                                       tgt=tgt, tgt_mask=tgt_mask,
                                       src_key_padding_mask=src_key_padding_mask,
                                       tgt_key_padding_mask=tgt_key_padding_mask,
                                       memory_key_padding_mask=src_key_padding_mask)
        out = torch.index_select(
            out, dim=0, index=step)
        out = torch.diagonal(out, dim1=0, dim2=1)
        out = out.t()
        return out

    def get_src_embedding(self, src):
        """Embeds the source tensor.

        Args:
            src (Tensor): source tensor (S,N).

        Returns:
            Tensor: embedded tensor (S,N,E).
        """
        src = self.src_emb(src)
        src = self.src_positional_encoding(src)
        return src

    def get_tgt_embedding(self, tgt):
        """Embeds the target tensor.

        The target has a shift of 1 due to -1 padding.

        Args:
            tgt (Tensor): target tensor (T,N).

        Returns:
            Tensor: embedded tensor (T,N,E).
        """
        tgt = self.tgt_emb(tgt+1)
        tgt = self.tgt_positional_encoding(tgt)
        return tgt
