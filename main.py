import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import torch

from actor_critic_handler import ActorCriticHandler
from environment import Environment, are_equal_reward, translate

sns.set_theme(style="darkgrid")
sns.set_palette("hls")


def collect_data(episode, encoder_value, log_prob, reward, tderr):
    return [{'episode': episode,
             'batch': batch,
             'encoder_value': ve,
             'log_prob': lg,
             'tderr': td,
             'reward': r} for batch, (ve, lg, r, td) in enumerate(zip(encoder_value,
                                                                      log_prob,
                                                                      reward,
                                                                      tderr))]


def run():
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(device)

    num_items = 2
    num_chars = 2
    list_max_len = 3
    sentence_max_len = 3

    global_list = []

    env = Environment(input_max_len=list_max_len)

    encoder_params = {"src_size": num_items,
                      "tgt_size": num_items,
                      "src_max_len": list_max_len,
                      "tgt_max_len": list_max_len,
                      "lr": 1e-4,
                      "lbd": 0,
                      "epsilon": 0.3,
                      "capacity": 1000,
                      "value_iter": 1,
                      "num_twins": 2,
                      "threshold": 0.5,
                      "embedding_dim": 100,
                      "nhead": 4,
                      "num_encoder_layer": 3,
                      "num_decoder_layer": 3,
                      "dropout": 0}
    encoder = ActorCriticHandler(device, **encoder_params)

    comb = [[1, 0, 0],
            [1, 1, 0],
            [1, 1, 1],
            [1, 1, 2],
            [1, 2, 0],
            [1, 2, 1],
            [1, 2, 2],
            [2, 0, 0],
            [2, 1, 0],
            [2, 1, 1],
            [2, 1, 2],
            [2, 2, 0],
            [2, 2, 1],
            [2, 2, 2]]

    encoder_input = torch.Tensor(comb).long().t().to(device)

    encoder.to(device)
    visualization_rate = 125
    for episode in range(1000):

        # ========== Intermediate Loops ==========
        # encoder_input = env.get_input_batch(num_items=2, bsz=5)

        encoder.collect_transitions(encoder_input)

        # The Encoder generates the sentence.

        encoder_output = encoder.actions

        # ========== Final Step ===========

        reward = are_equal_reward(encoder_input, encoder_output).unsqueeze(0)

        encoder_value = encoder.get_state_value(encoder_input, masked=False)

        tderr = reward - encoder_value

        encoder.store_last_transtion(encoder_input, reward, tderr)

        encoder.replay(500)
        # ====== Resetting agent inner state =====

        encoder.reset_state()

        # ======= Collecting data for data visualization ========

        global_list += collect_data(episode,
                                    encoder_value.squeeze(0).tolist(),
                                    (encoder.log_probs.sum(
                                        dim=0)/(encoder.actions != 0).sum(dim=0)).squeeze(0).tolist(),
                                    (reward).squeeze(0).tolist(),
                                    tderr.squeeze(0).tolist())


        if (episode+1) % visualization_rate == 0:
            print("Episode: ", episode)
            print("Ménestrel encode: \n", encoder_input)
            print("en: \n", encoder_output)
            print("or translated: \n", translate(encoder_output))
            print("and we have {} /14 success !".format(are_equal_reward(encoder_input,
                                                                         encoder_output,
                                                                         penalize=False).sum()))
            if episode+1 == visualization_rate:
                df = pd.DataFrame(global_list)
            else:
                df = df.append(global_list)
                df = df.reset_index(drop=True)
            df = df.astype({"batch": 'category'})
            global_list = []
            df = df.drop(columns=['cumsum',
                                  'average_encoder_value', 'average_log_prob'], errors='ignore')
            df['cumsum'] = df.groupby('batch').reward.cumsum()
            df['cumsum'] = df['cumsum'] / (df['episode']+1)
            def f(x): return x.rolling(window=50).mean()
            df['average_encoder_value'] = df.groupby(
                'batch').encoder_value.apply(f)
            df['average_log_prob'] = df.groupby(
                'batch').log_prob.apply(f)
            sns.relplot(x="episode", y="average_encoder_value", hue="batch",
                        kind="line", linewidth=2, ci=None,  data=df)
            sns.relplot(x="episode", y="average_log_prob", hue="batch",
                        kind="line", linewidth=2, ci=None,  data=df)
            # sns.relplot(x="episode", y="average_actor_loss", hue="batch", kind="line",
            #             linewidth=2, ci=None,  data=df)
            # sns.relplot(x="episode", y="critic_loss", hue="batch", kind="line", linewidth=2, ci=None, data=df)
            # sns.relplot(x="episode", y="tderr", hue="batch", kind="scatter", linewidth=2, ci=None, data=df)
            sns.relplot(x="episode", y="cumsum", hue="batch", kind="line",
                        linewidth=2, ci=None,  data=df)
            plt.show()


if __name__ == "__main__":
    run()
