"""Unit tests for environment
"""

import torch

import environment

env = environment.Environment(input_max_len=5)


def test_input_list():
    assert env.get_input(5).size() == torch.Size((5, 1))


def test_input_batch():
    assert env.get_input_batch(5, 4).size() == torch.Size((5, 4))


def test_step_encoder():
    assert torch.all(env.get_reward(torch.Tensor(
        [1, 1, 1]), agent_type='encoder') == \
            -torch.exp(-9 + torch.full((3,), 0, dtype=torch.float64)))


def test_step_decoder():
    assert torch.all(env.get_reward(torch.Tensor(
        [1, 1, 1]), agent_type='decoder') == torch.zeros((1, 3)))


def test_final_reward():
    assert env.contain_same_elements(
        torch.Tensor([1, 0, 2]), torch.Tensor([0, 2, 1]))


def test_translate():
    assert env.translate(torch.Tensor([[25, 14], [5, 15], [19, 0]])) == [
        'yes', 'no']
