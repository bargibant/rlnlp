"""Handler class for the actor-critic algorithm.
Actor and critic are two distincts models, both of them based on the nn.Transformer.
    For lighter signatures, all arguments are passed down through a kwargs
dictionnary.

    Possible arguments:
        src_size:
            number of possible indices in the source (default=2).
        tgt_size:
            number of possible indices in the target (default=26).
        src_max_len:
            max length of the source (default=5).
            See environment for more details about task inputs.
        tgt_max_len:
            max length of the target (default=5).

        lr:
            learning rate (default=1e-5).
        clip:
            gradient clipping (default=0, no clipping).
        weight_decay:
            L2 weight decay for Adam optimizer (default=0)
        epsilon:
            probability of sampling a non-optimal action
            in epsilon-greedy distribution (default=0.2).
        min_epsilon:
            min epsilon value when using adaptative epsilon
            (default=0).
        lbd:
            lambda for TD(lambda) (default=0.8).

        value_iter:
            the number of times critic is optimized before critic (default=1).
            See https://arxiv.org/pdf/1802.09477 for details.
        num_twins:
            the number of next_value computed. Min is taken across these values (default=1).
            See https://arxiv.org/pdf/1802.09477 for details.

        embedding_dim:
            the number of expected features in the encoder/decoder inputs (default=128).

        nhead:
            the number of heads in the multiheadattention models (default=8).
        num_encoder_layers:
            the number of sub-encoder-layers in the encoder (default=3).
        num_decoder_layers:
            the number of sub-decoder-layers in the decoder (default=3).
        dim_feedforward:
            the dimension of the feedforward network model (default=512).

        dropout:
            the dropout value (default=0).

        capacity:
            replay memory capacity (default=10).
        threshold:
            threshold to get into memory (default=0.1).
        ratio:
            the proportion of good episodes in a sampled batch (default=0.5).
"""
import torch
import torch.nn as nn

from models import Actor, Critic
# from optimizer import AdamElegibilityTraces
from optimizer import SGDElegibilityTraces
from replay_memory import BalancedMemory, Transition, ReplayMemoryBis


class ActorCriticHandler(nn.Module):
    """ Class to handle the Actor-Critic algorithm.
        S : source size,
        T : target size,
        N : batch size,
        E : embedding size.
    """

    def __init__(self, device, **kwargs):
        super().__init__()
        prop_defaults = {
            "epsilon": 0.2,
            "min_epsilon": 0,
            "clip": 0,
            "lr": 1e-5,
            "lbd": 0.8,
            "weight_decay": 0,
            "tgt_max_len": 5,
            "capacity": 10,
            "threshold": 0.1,
            "num_twins": 1,
            "value_iter": 1,
            "ratio": 0.5
        }
        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))

        self.device = device

        self.first_step = True
        self.step = None
        self.next_step = None
        self.done = torch.full((1, 1), True)
        self.next_done = torch.full((1, 1), True)

        self.actions = None
        self.actions_list: list = []
        self.log_probs = None
        self.log_probs_list: list = []

        self.tgt = None
        self.next_tgt = None

        self.memory = BalancedMemory(self.capacity)
        self.memory_trans = ReplayMemoryBis(self.capacity)

        self.actor = Actor(device=device, **kwargs)
        self.critic = Critic(device=device, **kwargs)

        self.actor_optimizer = SGDElegibilityTraces(
            self.actor.parameters(), lr=self.lr, lbd=self.lbd, weight_decay=self.weight_decay)
        self.critic_optimizer = SGDElegibilityTraces(
            self.critic.parameters(), lr=self.lr, lbd=self.lbd, weight_decay=self.weight_decay)

    def get_state_value(self, src, tgt=None, masked=True):
        """Computes the current state value (state is src + tgt)

        Args:
            src (Tensor): source tensor (S,N).
            masked (bool, optional): whether to return 0 if episode is terminated. Defaults to True.

        Returns:
            Tensor: state value tensor (1,N).
        """
        bsz = src.size(-1)
        if self.first_step:
            self.init_state(bsz)
        if tgt is None:
            tgt = self.pad_tgt(self.tgt, bsz)
            step = self.step
        else:
            step = self.compute_step(tgt)
        value = self.critic(src, tgt, step)
        if masked:
            value = value*(~self.done)
        return value

    def get_next_state_value(self, src, tgt=None, masked=True):
        """Computes the next state value (see Actor-Critic algorithm).

        Args:
            src (Tensor): source tensor (S,N).
            masked (bool, optional): whether to return 0 if next state is terminal.
            Defaults to True.

        Returns:
            Tensor: next state value tensor (1,N).
        """
        bsz = src.size(-1)
        if self.first_step:
            self.init_state(bsz)
        if tgt is None:
            tgt = self.pad_tgt(self.next_tgt, bsz)
            next_step = self.next_step
        else:
            next_step = self.compute_step(tgt) + 1
        value = self.critic(src, tgt, next_step)
        if masked:
            value = value*(~self.next_done)
        return value

    def apply_actor(self, src, tgt=None, epsilon=None, value=None, masked=True):
        """Computes action, its log probability and registers next state.

        Args:
            src (Tensor): source tensor (S,N).
            epsilon (float, optional): for the epsilon-greedy sampling. If None, uses init value.
                Defaults to None.
            value (Tensor, optional): if given, computes adaptative epsilon. See ideas.ipynb.
                Defaults to None.
            masked (bool, optional): Whether to cancel action and log_prob if episode is terminated.
                Defaults to True.

        Returns:
            (Tensor, Tensor)]: action and its log probability ((1,N), (1,N)).
        """
        bsz = src.size(-1)
        if self.first_step:
            self.init_state(bsz)
        if tgt is None:
            tgt = self.pad_tgt(self.tgt, bsz)
            step = self.step
        else:
            step = self.compute_step(tgt)
        if epsilon is None:
            epsilon = self.epsilon
        epsilon = torch.Tensor([
            epsilon]).to(self.device)
        if value is not None:
            epsilon = f_epsilon(value.detach().clone(),
                                epsilon, self.min_epsilon)

        action, log_prob = self.actor(src, tgt, step, epsilon)

        if masked:
            action = action*(~self.done)
            log_prob = log_prob*(~self.done)
        return action, log_prob

    def compute_step(self, tgt):
        step = (tgt!=0).logical_and(
            tgt!=-1).sum(dim=0).squeeze(0).to(self.device)
        return step

    def get_log_prob(self, src, action, tgt=None):
        """Computes log probability of action for current state.
        Used when episode is terminated to replay last transition.

        Args:
            src (Tensor): source tensor (S,N).
            action (Tensor): action (1,N).

        Returns:
            Tensor: log probability of action (1,N).
        """
        bsz = src.size(-1)
        if self.first_step:
            self.init_state(bsz)
        if tgt is None:
            tgt = self.pad_tgt(self.tgt, bsz)
            step = self.step
        else:
            step = self.compute_step(tgt)
        log_prob = self.actor.get_log_probs(src, tgt, step, action)
        return log_prob

    def push_to_memory(self, agent_input, reward, value=None):
        """Pushes an episode, that is agent input and its actions into memory for replay.

        If value is given, only episodes which ends with value < threshold are retained.
        This prioritizes rare successful episodes.

        Some random episodes are sampled also, to keep some bad episodes and prevent overfitting.

        Args:
            agent_input (Tensor): agent input for replay memory.
            value (Tensor, optional): State value. Defaults to None.
        """
        mask = (reward == 1).to(self.device)

        if value is not None:
            mask = (mask*(value < self.threshold).to(self.device))

        distrib = torch.distributions.bernoulli.Bernoulli(0.2)
        random_episodes_indices = distrib.sample(mask.size()).to(self.device)
        mask = mask.logical_or(random_episodes_indices)

        indices = torch.arange(reward.size(-1)).to(self.device)
        indices = torch.masked_select(indices, mask)
        agent_input = torch.index_select(agent_input, dim=-1, index=indices)
        actions = torch.index_select(self.actions, dim=-1, index=indices)
        reward = torch.index_select(reward, dim=-1, index=indices)
        value = torch.index_select(value, dim=-1, index=indices)
        for i_input, i_actions, i_reward, i_value in zip(agent_input.t(),
                                                         actions.t(),
                                                         reward.t(),
                                                         value.t()):
            if i_actions.size(0) > 0:
                if i_reward.item() == 1:
                    self.memory.push_success(
                        i_input.clone(),
                        i_actions.clone(),
                        i_reward.long().clone(),
                        i_value.clone())
                else:
                    self.memory.push_fail(
                        i_input.clone(),
                        i_actions.clone(),
                        i_reward.long().clone(),
                        i_value.clone())

    def replay_actor(self, src, action):
        """Computes log probability for given action and computes next state.

        Args:
            src (Tensor): source tensor (S, N).
            action (Tensor): action tensor (S, N).

        Returns:
            Tensor: log probability of action.
        """
        log_prob = self.get_log_prob(src, action)
        return log_prob*(~self.done)

    def replay(self, batch_size, ratio=None):

        if ratio is None:
            ratio = self.ratio

        batch_size = min(len(self.memory_trans), batch_size)

        transitions = self.memory_trans.sample(batch_size)
        batch = Transition(*zip(*transitions))

        final_tgt = torch.Tensor([-1], device=self.device).long()

        non_final_mask = torch.Tensor(tuple(map(lambda s: not s.equal(final_tgt),
                                                batch.next_tgt)), device=self.device).bool().unsqueeze(0)

        non_final_next_tgt = torch.stack(
            [s for s in batch.next_tgt if not s.equal(final_tgt)]).t()

        non_final_indices = torch.arange(batch_size).to(self.device)
        non_final_indices = torch.masked_select(non_final_indices, non_final_mask)

        src_batch = torch.stack(batch.src).t()
        tgt_batch = torch.stack(batch.tgt).t()
        action_batch = torch.stack(batch.action).t()
        reward_batch = torch.stack(batch.reward).t()

        value = self.get_state_value(src_batch, tgt_batch, masked=False)


        next_value = torch.zeros_like(value)

        non_final_src = src_batch.index_select(dim=-1, index=non_final_indices)
        next_value[non_final_mask] = self.get_next_state_value(
            non_final_src, non_final_next_tgt, masked=False)

        tderr = reward_batch + next_value - value

        log_prob = self.get_log_prob(src_batch, action_batch, tgt_batch)

        # next_values = torch.cat([self.get_next_state_value(
        #     agent_input) for _ in range(self.num_twins)], dim=0)
        # next_value, _ = torch.min(next_values, dim=0)

        critic_loss = -tderr.detach()*value
        self.optim_critic(critic_loss, masked=False)

        actor_loss = -tderr.detach()*log_prob

        self.optim_actor(actor_loss, masked=False)


    def collect_transitions(self, src, agent_type='encoder', adapt_epsilon=True):
        """Intermediate training loop, to generate agent output but keeping the last transition
        on standby.

        Args:
            agent_input (Tensor): Agent input.
            agent_type (str, optional): Whether 'encoder' or 'decoder. Defaults to 'encoder'.
            adapt_epsilon (bool, optional): Whether to use adaptative epsilon (see ideas.ipynb).
                Defaults to True.

        Returns:
            Tensor: action (1,N).
        """
        for _ in range(self.tgt_max_len + 1):
            
            value = self.get_state_value(src)
            if adapt_epsilon:
                action, log_prob = self.apply_actor(
                    src, value=value)
            else:
                action, log_prob = self.apply_actor(src)

            self.compute_next_state(action)
            self.register_action(action, log_prob)

            reward = self.get_intermediate_reward(
                action, agent_type).to(self.device)
            reward = reward*self.next_done

            next_value = self.get_next_state_value(src)

            tderr = reward + next_value - value

            batch_size = self.tgt.size(-1)

            tgt = self.pad_tgt(self.tgt, batch_size)
            next_tgt = self.pad_tgt(self.next_tgt, batch_size)

            transition = Transition(src, tgt, action, next_tgt, reward)

            mask = self.sampling_mask(transition, 0.6)

            mask = mask.logical_and(self.next_done == False)

            self.store_transition(transition, tderr, mask)

            self.update_state()

        return action

    def sampling_mask(self, transition, sampling_rate=1):
        distrib = torch.distributions.bernoulli.Bernoulli(sampling_rate)
        mask = (distrib.sample(transition.reward.size())==1).to(self.device)
        return mask
    
    def store_last_transtion(self, src, reward, tderr):
        
        tgt = self.pad_tgt(self.tgt, src.size(-1))
        action = torch.zeros((1, src.size(-1))).to(self.device)
        next_tgt = torch.full_like(action, -1).long()
        transition = Transition(src, tgt, action, next_tgt, reward)

        mask = self.sampling_mask(transition, 0.6)

        self.store_transition(transition, tderr, mask)

    def store_transition(self, transition, tderr, mask):

        indices = torch.arange(transition.tgt.size(-1)).to(self.device)
        indices = torch.masked_select(indices, mask)

        src = torch.index_select(
            transition.src, dim=-1, index=indices)
        tgt = torch.index_select(
            transition.tgt, dim=-1, index=indices)
        action = torch.index_select(
            transition.action, dim=-1, index=indices)
        next_tgt = torch.index_select(
            transition.next_tgt, dim=-1, index=indices)
        reward = torch.index_select(
            transition.reward, dim=-1, index=indices)
        tderr = torch.index_select(tderr, dim=-1, index=indices)

        for i_src, i_tgt, i_action, i_next_tgt, i_reward, i_tderr in zip(src.t(),
                                                                         tgt.t(),
                                                                         action.t(),
                                                                         next_tgt.t(),
                                                                         reward.t(),
                                                                         tderr.t()):

            self.memory_trans.push(Transition(i_src,
                                              i_tgt,
                                              i_action,
                                              i_next_tgt,
                                              i_reward),
                                   torch.abs(i_tderr))

    def replay_memory(self, sampling_size, ratio=None, agent_type="encoder"):
        """Replay episodes with good final reward.

        Args:
            ratio (float): percentage of good episode in sampled batch.
            sampling_size (float): number of episodes to replay.
            agent_type (str, optional): Whether 'encoder' or 'decoder. Defaults to "encoder".
        """
        if ratio is None:
            ratio = self.ratio

        if len(self.memory) == 0:
            return
        agent_input, agent_actions, agent_reward = self.memory.sample(
            sampling_size, ratio)
        padding = torch.zeros((1, agent_actions.size(-1))
                              ).long().to(self.device)
        agent_actions = torch.cat([agent_actions, padding], dim=0)

        for value_iter in range(self.value_iter):
            self.critic_optimizer.delete_traces()

            for action in agent_actions:
                action = action.unsqueeze(0)

                value = self.get_state_value(agent_input)

                log_prob = self.replay_actor(agent_input, action)
                self.compute_next_state(action)

                reward = self.get_intermediate_reward(
                    action, agent_type).to(self.device)
                reward = reward*self.next_done

                next_values = torch.cat([self.get_next_state_value(
                    agent_input) for _ in range(self.num_twins)], dim=0)
                next_value, _ = torch.min(next_values, dim=0)

                tderr = reward + next_value - value

                critic_loss = -tderr.detach()*value
                self.optim_critic(critic_loss)

                if value_iter == self.value_iter-1:
                    actor_loss = -tderr.detach()*log_prob
                    self.optim_actor(actor_loss)
                self.update_state()

            reward = agent_reward
            action = torch.zeros_like(value)

            agent_value = self.get_state_value(agent_input, masked=False)

            log_prob = self.get_log_prob(agent_input, action)

            tderr = reward - agent_value

            critic_loss = -tderr.detach()*agent_value

            self.optim_critic(critic_loss, masked=False)

            if value_iter == self.value_iter-1:
                actor_loss = -tderr.detach()*log_prob
                self.optim_actor(actor_loss, masked=False)
            self.reset_state()

    def forward(self, src):
        """Predict next state and action for inference.

        Args:
            src (Tensor): source tensor (S,N).

        Returns:
            (Tensor, Tensor): action and its logprobability ((1,N), (1,N)).
        """
        bsz = src.size(-1)
        if self.first_step:
            self.init_state(bsz)
        tgt = self.pad_tgt(self.tgt, bsz)

        action, log_prob = self.actor(
            src, tgt, self.step, epsilon=torch.Tensor([0]).to(self.device))

        self.compute_next_state(action)
        self.register_action(action, log_prob)
        action = action*(~self.done)
        log_prob = log_prob*(~self.done)
        return action, log_prob

    def compute_next_state(self, action):
        """Compute the done, step and tgt for the next state.
        Next state is terminal if action is 0 or the next step is
        the last possible.

        Args:
            action (Tensor): action (1,N).
            log_prob (Tensor): log probability (1, N).
        """
        self.next_done = self.done.logical_or(torch.full((1, self.tgt.size(-1)), (self.tgt.size(0) == self.tgt_max_len+1))
                                              .logical_or((action == 0).squeeze(0)))
        self.next_step = self.step + (~self.next_done).long()
        self.next_step = self.next_step.squeeze(0)

        padding = torch.full((1, self.tgt.size(-1)), -1,
                             dtype=torch.long).to(self.device)
        zero_padding = torch.zeros(
            (1, self.tgt.size(-1)), dtype=torch.long).to(self.device)

        truc = ((~self.next_done).logical_and(~self.done)).long()*action*(~self.done).long() + \
            ((self.next_done).logical_and(~self.done)).long()*zero_padding + \
            (self.done).long()*padding

        self.next_tgt = torch.cat([self.tgt, truc], dim=0)

    def update_state(self):
        """Update the target, step and done tensor.
        Target is updated only if next state is not terminal.
        """
        self.tgt = self.next_tgt.detach().clone()
        self.step = self.next_step.detach().clone()
        self.done = self.next_done.detach().clone()


    def register_action(self, action, log_prob):
        """ Generates the actions and log-probs tensors, which contain all actions so far,
        and hence, the real output of the mode, with the corresponding log-probs.

        Args:
            action (Tensor]):  action (1,N).
            log_prob (Tensor]): log probability (1, N).
        """
        mask = (~self.done).long()
        if self.next_tgt.size(0) < self.tgt_max_len + 2:
            self.actions_list.append(action*mask)
            self.actions = torch.cat(self.actions_list, dim=0)
            self.log_probs_list.append(log_prob*mask)
            self.log_probs = torch.cat(self.log_probs_list, dim=0)

    def flush_memory(self):
        """Deletes memory."""
        self.memory.flush()

    def collect_data(self, agent_input, adapt_epsilon=True):
        """Intermediate training loop, to generate agent output but keeping the last transition
        on standby.

        Args:
            agent_input (Tensor): Agent input.
            agent_type (str, optional): Whether 'encoder' or 'decoder. Defaults to 'encoder'.
            adapt_epsilon (bool, optional): Whether to use adaptative epsilon (see ideas.ipynb).
                Defaults to True.

        Returns:
            Tensor: action (1,N).
        """
        with torch.no_grad():
            for _ in range(self.tgt_max_len + 1):

                value = self.get_state_value(agent_input)

                if adapt_epsilon:
                    action, log_prob = self.apply_actor(
                        agent_input, value=value)
                else:
                    action, log_prob = self.apply_actor(agent_input)

                self.compute_next_state(action)
                self.register_action(action, log_prob)
                self.update_state()

        value = self.get_state_value(agent_input, masked=False)

        return value

    def intermediate_train_loop(self, agent_input, agent_type='encoder', adapt_epsilon=True):
        """Intermediate training loop, to generate agent output but keeping the last transition
        on standby.

        Args:
            agent_input (Tensor): Agent input.
            agent_type (str, optional): Whether 'encoder' or 'decoder. Defaults to 'encoder'.
            adapt_epsilon (bool, optional): Whether to use adaptative epsilon (see ideas.ipynb).
                Defaults to True.

        Returns:
            Tensor: action (1,N).
        """
        for _ in range(self.tgt_max_len + 1):

            for _ in range(self.value_iter):
                value = self.get_state_value(agent_input)

                if adapt_epsilon:
                    action, log_prob = self.apply_actor(
                        agent_input, value=value)
                else:
                    action, log_prob = self.apply_actor(agent_input)

                self.compute_next_state(action)
                self.register_action(action, log_prob)

                reward = self.get_intermediate_reward(
                    action, agent_type).to(self.device)
                reward = reward*self.next_done

                next_values = torch.cat([self.get_next_state_value(
                    agent_input) for _ in range(self.num_twins)], dim=0)
                next_value, _ = torch.min(next_values, dim=0)

                tderr = reward + next_value - value

                critic_loss = -tderr.detach()*value
                self.optim_critic(critic_loss)

            actor_loss = -tderr.detach()*log_prob

            self.optim_actor(actor_loss)

            self.update_state()

        return action

    def get_intermediate_reward(self, action, agent_type='encoder'):
        """ Returns reward according to agent type:
         - encoder: decreasing reward by time,
         - decoder: no intermediate reward.

        Args:
            agent_type (str): whether 'encoder' or 'decoder.

        Returns:
            Tensor: reward.
        """
        bsz = action.size(-1)

        if agent_type == 'encoder':
            reward = -torch.exp(-1.5*self.tgt_max_len + self.step)
        elif agent_type == 'decoder':
            reward = torch.zeros((1, bsz))
        return reward

    def optim_step(self, critic_loss, actor_loss, masked=True):
        """Triggers the optimizer.step() according to critic and actor loss.

        Args:
            critic_loss (Tensor): (1,N).
            actor_loss (Tensor): (1,N).
            masked (bool, optional): Whether not to compute the last step.
            Defaults to True.
        """
        self.optim_critic(critic_loss, masked)
        self.optim_actor(actor_loss, masked)

    def optim_actor(self, actor_loss, masked=True):
        """Triggers the optimizer.step() for the actor.

        Args:
            actor_loss (Tensor): (1,N).
            masked (bool, optional): Whether not to compute the last step.
            Defaults to True.
        """
        if masked:
            actor_loss.register_hook(
                lambda grad: grad*(~self.next_done).float())

        self.actor_optimizer.zero_grad()
        actor_loss.sum().backward()
        if self.clip != 0:
            torch.nn.utils.clip_grad_value_(self.actor.parameters(), self.clip)
        self.actor_optimizer.step(masked=masked)

    def optim_critic(self, critic_loss, masked=True):
        """Triggers the optimizer.step() for the critic.

        Args:
            critic_loss (Tensor): (1,N).
            masked (bool, optional): Whether not to compute the last step.
            Defaults to True.
        """
        if masked:
            critic_loss.register_hook(
                lambda grad: grad*(~self.next_done).float())

        self.critic_optimizer.zero_grad()
        critic_loss.sum().backward()
        if self.clip != 0:
            torch.nn.utils.clip_grad_value_(
                self.critic.parameters(), self.clip)
        self.critic_optimizer.step(masked=masked)

    def pad_tgt(self, tgt, bsz):
        """Pad the target with -1.

        Args:
            tgt (Tensor): target tensor (step+1,N).
            bsz (int): batch size.

        Returns:
            Tensor: padded target (T,N).
        """
        padding = torch.full((self.tgt_max_len + 2 - tgt.size(0), bsz), -1,
                             dtype=torch.long).to(self.device)
        tgt = torch.cat([tgt.clone(),
                         padding],
                        dim=0)
        return tgt

    def init_state(self, bsz):
        """Initializes all utility tensors.

        Args:
            bsz (int): batch size.
        """
        self.init_tgt(bsz)
        self.init_done(bsz)
        self.init_step(bsz)
        self.first_step = False

    def init_tgt(self, bsz):
        """Initializes the target. -1 is the padding index.

        Args:
            bsz (int): batch size.
        """
        tgt = torch.full((1, bsz), -1, dtype=torch.long).to(self.device)
        self.tgt = tgt.clone()
        self.next_tgt = self.tgt.clone()

    def init_step(self, bsz):
        """Initializes the step.

        Each tensor of the batch has its own step, which stops being incremented
        when episode is terminated.

        Args:
            bsz (int): batch size.
        """
        self.step = torch.zeros((bsz,), dtype=torch.long).to(self.device)
        self.next_step = self.step.clone()

    def init_done(self, bsz):
        """Initializes the done tensor.

        Args:
            bsz (int): batch size.
        """
        self.done = torch.full((bsz,), False).to(self.device)
        self.next_done = self.done.clone()

    def reset_state(self):
        """ Makes first_step True to trigger state initialization."""
        self.first_step = True
        self.actions_list = []
        self.log_probs_list = []


def f_epsilon(value, epsilon, min_epsilon):
    """Computes adaptative epsilon. See ideas.ipynb.

    Args:
        value (Tensor): state value (1,N).
        epsilon (float): max epsilon value.

    Returns:
        Tensor: adaptative epsilon (1,N).
    """
    value = value-1
    value = value.pow(2)
    adapted_epsilon = (value/(value+1))*epsilon
    adapted_epsilon = 1-(1-min_epsilon)*(1-adapted_epsilon)
    return adapted_epsilon
