# Emergence of language through multi-agent RL based Autoencoder

**What is a language?**

![](images/SceneNoBackground.png)

From a very pragmatical point of view, it's a mapping from *ideas, objects, concepts*, to some sequence of characters, *a sentence*, so that one can understand which ideas, objects it represents.

With this in mind, two agents could develop their own language by agreeing on the words used when confronted to something. 

Well, this is the common idea of how the first words emerged. A primitive guy may have said "*Yabba Dabba Doo !*" and got a nice flintstone.

This project intends to make two Reinforcement Learning agents replicate this language creation process.

## Task

More Reinforcement-Learningly speaking, one agent has to encode a list of many possible elements, or items (1000 seems reasonable) into a sentence (list of 26 possible characters), and the other agent will try to restaure the initial list.

The actions are:
- For the Encoder agent, the 26 characters,
- For the Decoder agent, the 1000 possible items.

![](images/task.png)

Taking a '0' causes the sentence and output list to end.

Both agents get a reward if input and output lists match.

Some intermediate rewards could be given to the encoder in order to generate shorter sentences. 

The input and the sentence may contain any number of elements, with fixed max length.

An episode terminates when both agents terminated their output sequence.

Two tasks are studied: 
- input and output lists are strictly identical,
- input and output lists contain same items.

I expect first task to work but show no particular language intelligence, whereas second task could make the model learn how to count and to name items.


## Development

For the main characteristics:
- Both agents have the same scheme for generating their output,
![The Encoder structure](images/generating_output.png)
    - The first agent encodes the input list into memory,
    - Then it decodes iteratively the sentence based on the memory and the current output characters.
    - the second agent takes this output sentence to restaure the input list.

- The agents' model is based on the Transformer from the famous ["Attention is all you need"](https://arxiv.org/abs/1706.03762),
- Actor-Critic with TD(lambda) is used, 
- Within an agent, the actor and the critic don't share their weights for practical reasons.

Basically, the pair Encoder/Decoder has an [Autoencoder](https://en.wikipedia.org/wiki/Autoencoder) structure. It differs though, in that there is no gradient propagating through both the Encoder and the Decoder. Each agent are trained individually, they only share the same final reward.

*Some development ideas are detailed in `ideas.ipynb`.*

*For more details about the Transformer, read [The Illustrated Transformer](http://jalammar.github.io/illustrated-transformer/), J.Alammar.*


The model is mostly trained on Google Colab.

## Results

The file *overfit.ipynb* trains a single agent. This agent tries to give back its input. This *overfitting* shows that the model is able to learn something and converges.

 ![](images/reward.png)
 
(average_reward here is just a windowed reward, not the reward on multiple runs)

![](images/encoding.png)

However, the model seems to suffer from a *big crunch*... Though convergence seems persistent, after some episodes, the model's performance collapses.

 ![](images/reward_collapse.png)

Before that, some pikes occur, and although it leads to better results, it looks quite drastical, and seems to slow learning.

The reason for this seems to be the little increase in the TD error:

<img src="images/culprit.png" alt="drawing" width="200"/>

At some point, the TD error indicates that the next reward estimate is better than the current one (V), so that there is a better policy. 

After some research, divergence seems to be caused by overestimated value, as detailed in 
[Addressing Function Approximation Error in Actor-Critic Methods](https://arxiv.org/pdf/1802.094771), so next step is to implement a more stable Actor-Critic algorithm.

 ![](images/EndScene.png)

## Updates

   - Currently shifting from episode based memory to transition based memory, hoping it will reduce overfitting by decorrelating transitions from episodes.
   - While implementing my memory, I found out that this [paper](https://arxiv.org/pdf/1511.05952) actually uses similar ideas: sample episodes based on TD-error, for example.
   - Now that only the memory is used, some overfitting appears. A prioritized memory, which favors hesitating episodes will be implemented.
   - The model was trained online and with memory. Now, only memory is used, for speed reasons. 
   - A simple replay memory is bad, since most episodes will be unsuccessful.
    Instead, a balanced memory is implemented. Sampled batchs have a set ratio of successful and
    failed episodes. 



## Author (and Artist)

Guillaume Bargibant

guillaume3136@live.fr

 www.linkedin.com/in/guillaume-bargibant-32a499172





